package com.mycompany.projects;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) {

		App newApp = new App();
		saludoEstatico();
		System.out.println(saludoMetodoTipo());
		System.out.println(newApp.saludoInstancia());
	}

	public static void saludoEstatico() {
		System.out.println("Hola Mundo desde metodo estatico");
	}

	public static String saludoMetodoTipo() {
		return "Hola Mundo Metodo que regresa String";
	}

	public String saludoInstancia() {
		return "Hola Mundo desde Metodo Instancia";
	}
}
