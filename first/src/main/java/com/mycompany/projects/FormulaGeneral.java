package com.mycompany.projects;

import java.util.Scanner;

public class FormulaGeneral {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		FormulaGeneral formula = new FormulaGeneral();

		String resp;

		do {
			System.out.println("Ingresa el valor de A");
			double a = scanner.nextDouble();

			System.out.println("Ingresa el valor de B");
			double b = scanner.nextDouble();

			System.out.println("Ingresa el valor de C");
			double c = scanner.nextDouble();

			formula.calcularFormula(a, b, c);

			System.out.println("Calcular con otros valores? Y/N");
			resp = scanner.next();
		} while (resp.equalsIgnoreCase("Y"));

		System.out.println("Finish");
	}

	public void calcularFormula(double a, double b, double c) {

		double d = Math.pow(b, 2) - 4 * a * c;

		if (d < 0) {
			System.out.println("La soluci�n no es real");
		} else {
			double x1 = (-b + Math.sqrt(d)) / 2 * a;
			double x2 = (-b - Math.sqrt(d)) / 2 * a;

			System.out.println("");
			System.out.println("Soluci�n:\n x1:" + x1 + "\n x2:" + x2);
		}

	}

}
